import {Pipe, PipeTransform} from "@angular/core";
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(value: any, searchKeyword: string, searchArgs: string[]): any {
    if(!searchKeyword) return value;
    searchKeyword = searchKeyword.toLowerCase();
    console.log(searchKeyword, ".");
    return value.filter(i => {
      let found = false;
      searchArgs.forEach(arg => {
        console.log(i[arg], searchKeyword);
        if (i[arg] && i[arg].toLowerCase().indexOf(searchKeyword) != -1) {
          found = true;
        }
      })
      return found;
    })
  }
}
