/**
 * Created by denkomanceski on 5/30/17.
 */

import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable, Subscription} from "rxjs";
import {UtilService} from "./utilities";
import {PaymentRequestComponent} from "../pages/payment-request/payment-request";
import {Push} from "@ionic/cloud-angular";
import {ModalController} from "ionic-angular";
import {PushToken} from "@ionic/cloud/dist/es5";

@Injectable()
export class UserService {
  paymentPollSub: Subscription;
  accounts: any = [];
  filesToUpload: any = [];
  private pushSubscription: Subscription;
  private activePaymentPending = {};

  constructor(private http: Http, private util: UtilService, private modal: ModalController) {
    // this.init();
  }

  friendList = [];
  payments = [];
  lifeEvents = [];
  user: any = {};
  //userId = '592d7fb1ed1ac20e3681ab6a';
  registerPush(push: Push) {
    push.register().then((t: PushToken) => {
      return push.saveToken(t);
    }).then((t: PushToken) => {
      console.log('Token saved:', t.token);
      this.saveNotifsToken(t.token).subscribe(data => {
        console.log("Token uploaded to the server.");
      });
    });
    if (this.pushSubscription) this.pushSubscription.unsubscribe();
    this.pushSubscription = push.rx.notification().subscribe(data => {
      console.log("GOT A NOTIFICATION.", JSON.stringify(data));
      let modal = this.modal.create(PaymentRequestComponent, {data, userService: this});
      modal.present();
    })
  }

  init() {
    console.log("Initialize.");
    this.lifeEvents = this.user.lifeEvents;
    this.friendList = this.user.friends;
    console.log(this.lifeEvents, this.friendList, "???");

    this.getPayments().subscribe(data => {
      this.payments = data;
    }, UtilService.handleError);
    this.getAccounts().subscribe(accounts => {
      this.accounts = accounts;
    })
    //this.registerPush();
  }

  getLifeEvents() {
    return this.util.get('users/lifeevents?userId=' + this.user.userId)
  }

  getPayments() {
    return this.util.get('payments/payments?userId=' + this.user.userId)
  }

  getAccounts() {
    return this.util.get('accounts?userId=' + this.user.userId);
  }

  getPaymentById(paymentId: string) {
    return this.util.get('payments/payment?paymentId='+paymentId)
  }

  getFriendlist() {
    return Observable.of([
        {
          userId: '592d7fb3ed1ac20e3681ab6a',
          firstName: 'Denko',
          lastName: 'Mancheski',
          birthDate: new Date(),
          socialToken: 'qweqweqwe',
          fingerPrintMode: false,
          phoneNumber: 'Denko',
          createDate: new Date(),
          accounts: [
            {
              //TBD
            }
          ],
          lifeEvents: [
            {
              name: 'Birthday',
              occurDate: new Date()
            }
          ]
        },
        {
          userId: '592d7fb1ed1ac20e3681ab6c',
          firstName: 'Dario',
          lastName: 'Mirkovski',
          birthDate: new Date(),
          socialToken: 'qweqweqwe',
          fingerPrintMode: false,
          phoneNumber: '123123132',
          createDate: new Date(),
          accounts: [
            {
              //TBD
            }
          ],
          lifeEvents: [
            {
              name: 'Birthday',
              occurDate: new Date()
            }
          ]
        }

      ]
    )
  }

  removeEvent(eventId: any) {
    return this.util.del(`lifeEvent`)
  }

  saveUserInfo() {
    console.log(this.user, ".....");
    return this.util.put('users/user', this.user)
  }

  getUserInfo(userId: string) {
    return this.util.get('users/user?userId=' + userId)
  }

  sendFacebookInfo(info) {


    return this.util.post('users/connectfb', {
      facebookId: info.authResponse.userID,
      accessToken: info.authResponse.accessToken,
      userId: this.user.userId
    })
  }

  initiatePayment(payment: any) {
    return this.util.post('payments/initiate', payment)
  }

  uploadBill() {
    //this.http.post('')
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;

    formData.append("sampleFile", files[0], files[0]['name']);
    this.http.post(this.util.serverUrl + '/bills/upload', formData)
      .map(files => files.json())
      .subscribe(files => console.log('files', files))
  }

  saveLifeEvent(newEvent: any) {
    console.log(newEvent, "??");
    newEvent.userId = this.user.userId;
    return this.util.post('users/lifeevent', newEvent)
  }

  deleteLifeEvent(eventId: string) {
    return this.util.del('users/lifeevent?lifeEventId=' + eventId + '&userId=' + this.user.userId);
  }

  confirmPayment(payment: any) {
    return this.util.post('payments/confirm', payment)
  }

  declinePayment(payment) {

  }

  saveNotifsToken(ionicToken: string) {
    return this.util.post('users/saveionic', {ionicToken, userId: this.user.userId})
  }

  login(firstName: string) {
    return this.util.post('accounts/login', {firstName})
  }

  paymentPoll(paymentId: any) {
    console.log("Starting polling :", paymentId);
    this.cancelPaymentPoll();
    this.paymentPollSub = Observable.timer(0, 1000).subscribe(() => {
      console.log("Fetched...");
      this.getPaymentById(paymentId).subscribe(data => {
        console.log(data, "data...");
        this.activePaymentPending = data;
      })
    })
  }

  cancelPaymentPoll() {
    if (this.paymentPollSub)
      this.paymentPollSub.unsubscribe();
  }
}
