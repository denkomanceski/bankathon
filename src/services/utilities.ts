/**
 * Created by denkomanceski on 5/30/17.
 */
import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http"
import {UserService} from "./user.service";
@Injectable()
export class UtilService {
  serverUrl = 'http://192.168.43.144:8080';
  headers = new Headers({'content-type': 'application/json'});

  constructor(private http: Http){}
  get(url: string){
    return this.http.get(`${this.serverUrl}/${url}`, {headers: this.headers}).map(data => data.json())
  }
  post(url: string, data: any){
    console.log(data, "data.");
    //data.userId = this.userService.userId;
    return this.http.post(`${this.serverUrl}/${url}`, JSON.stringify(data), {headers: this.headers}).map(data => data.json())
  }
  put(url: string, data: any){
    //data.userId = this.userService.userId;
    return this.http.put(`${this.serverUrl}/${url}`, JSON.stringify(data), {headers: this.headers}).map(data => data.json())
  }
  del(url: string){
    return this.http.delete(`${this.serverUrl}/${url}`, {headers: this.headers}).map(data => data.json())
    //this.http.delete()
  }
  static handleError(err){
    console.error("An ERROR Occurred: ", JSON.stringify(err));
  };
}
