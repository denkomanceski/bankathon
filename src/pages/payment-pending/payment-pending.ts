import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {PaymentDetails} from "../payment-details/payment-details";

@Component({
  selector: 'page-payment-pending',
  templateUrl: 'payment-pending.html'
})
export class PaymentPendingPage {

  constructor(public navCtrl: NavController, private userService: UserService) {

  }
  openPayment(payment: any){
    this.navCtrl.push(PaymentDetails, payment)
  }
  ngOnDestroy(){
    this.userService.cancelPaymentPoll();
  }
}
