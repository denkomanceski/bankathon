import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {PaymentDetails} from "../payment-details/payment-details";
import {TabsPage} from "../tabs/tabs";
import {Push} from "@ionic/cloud-angular";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  firstName: any = '';

  constructor(public navCtrl: NavController, private userService: UserService, private push: Push) {

  }
  login(){
    this.userService.login(this.firstName).subscribe(data => {
      Object.assign(this.userService.user, data);
      this.userService.user.userId = data._id;
      this.userService.init();
      this.userService.registerPush(this.push);
      this.navCtrl.setRoot(TabsPage)
    }, err => {
      console.log(err, "error..");
    })
  }
}
