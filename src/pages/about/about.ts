import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {PaymentDetails} from "../payment-details/payment-details";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, private userService: UserService) {

  }
  openPayment(payment: any){
    this.navCtrl.push(PaymentDetails, payment)
  }
}
