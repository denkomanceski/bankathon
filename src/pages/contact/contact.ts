import { Component } from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {CreateLifetimeEventComponent} from "../create-lifetime-event/create-lifetime-event";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public userService: UserService, public modal: ModalController, private fb: Facebook) {
    //this.fb.browserInit(292836527793368);
  }
  facebookInit(){
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
      console.log("???");
        this.userService.sendFacebookInfo(res)
          .subscribe(data => {
            console.log("fetched.");
            //this.userService.getFriendlist().subscribe(data => this.userService.friendList = data);
        }, err => {
            console.log(JSON.stringify(err), "Err");
          })
      })
      .catch(e => console.log('Error logging into Facebook', e));

  }
  createNewEvent(){
    let modal = this.modal.create(CreateLifetimeEventComponent);
    modal.present();
  }
  removeEvent(eventId){
    this.userService.deleteLifeEvent(eventId).subscribe(event => {
      this.userService.getLifeEvents().subscribe(events => {
        this.userService.lifeEvents = events;
      });
    })
  }
}
