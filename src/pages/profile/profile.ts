import { Component } from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
import {InitiatePaymentComponent} from "../initiate-payment/initiate-payment";
import {EditProfilePage} from "../edit-profile/edit-profile";
import {UtilService} from "../../services/utilities";
import {UserService} from "../../services/user.service";
import {LoginPage} from "../login/login";

@Component({
  selector: 'profile-home',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public userService: UserService, private modal: ModalController) {

  }

  logout(){
    this.userService.user = {};
    this.navCtrl.setRoot(LoginPage);
  }

  editInfo(){
    let modal = this.modal.create(EditProfilePage);
    modal.present();
  }
}
