import { Component } from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
import {InitiatePaymentComponent} from "../initiate-payment/initiate-payment";
import {PaymentRequestComponent} from "../payment-request/payment-request";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  filesToUpload: File[];

  constructor(public navCtrl: NavController, private nav: NavController, private modal: ModalController,
  private userService: UserService) {

  }

  openPaymentModal(){
    let modal = this.modal.create(InitiatePaymentComponent);
    modal.present();
  }
  openRequestModal(){
    let modal = this.modal.create(PaymentRequestComponent);
    modal.present();
  }
  fileChangeEvent(fileInput: any) {
    this.userService.filesToUpload = <Array<File>>fileInput.target.files;
    this.userService.uploadBill()
    //this.product.photo = fileInput.target.files[0]['name'];
  }
}
