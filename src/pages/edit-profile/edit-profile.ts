import { Component } from '@angular/core';
import {NavController, ViewController} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {PaymentDetails} from "../payment-details/payment-details";

@Component({
  selector: 'page-edit',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage {

  constructor(public navCtrl: NavController, private userService: UserService, private view: ViewController) {

  }
  openPayment(payment: any){
    this.navCtrl.push(PaymentDetails, payment)
  }
  saveInfo(){
    this.userService.saveUserInfo().subscribe(data => {
      this.view.dismiss();
    })
  }
  dismiss(){
    this.view.dismiss();
  }
}
