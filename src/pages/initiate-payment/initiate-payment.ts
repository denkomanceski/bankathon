/**
 * Created by denkomanceski on 5/30/17.
 */
import {Component} from "@angular/core";
import {UserService} from "../../services/user.service"
import {ModalController, ViewController} from "ionic-angular";
import {PaymentPendingPage} from "../payment-pending/payment-pending";
@Component({
  selector: 'initiate-payment',
  templateUrl: 'initiate-payment.html'
})
export class InitiatePaymentComponent {

  note: string;
  amount: number = 0;
  searchWord: string = '';
  selectedUserIds = [];
  bankAccountId: string = '';
  step = 0;

  constructor(private userService: UserService, private view: ViewController, private modal: ModalController){
    console.log("Initialized");
  }

  filterFriendlist(evt){
    this.searchWord = evt.target.value;
  }
  incrementAmount(){
    this.amount++;
  }
  decrementAmount(){
    if(this.amount > 0){
      this.amount--;
    }
  }
  friendSelected(evt, userId){
    let flag = evt.value;
    console.log(evt.value, "hello.");
    if(flag && this.selectedUserIds.indexOf(userId) == -1){
      this.selectedUserIds.push(userId)
    } else {
      this.selectedUserIds.splice(this.selectedUserIds.indexOf(userId), 1)
    }
  }
  checkSelected(userId){
    return this.selectedUserIds.indexOf(userId) != -1;
  }
  initiatePayment(){
    this.userService.initiatePayment({
      amount: this.amount,
      userIds: this.selectedUserIds,
      userId: this.userService.user.userId,
      firstName: this.userService.user.firstName,
      lastName: this.userService.user.lastName,
      note: this.note,
      bankAccountId: this.bankAccountId
    }).subscribe(data => {
      console.log(JSON.stringify(data), "????");
      this.userService.paymentPoll(data.paymentId);
      this.view.dismiss();
      let modal = this.modal.create(PaymentPendingPage);
      modal.present();
      console.log("Payment initiated..");
    })
  }
  dismiss(){
    this.view.dismiss();
  }
  nextStep(){
    this.step++;
  }
  previousStep(){
    this.step--;
  }
}
