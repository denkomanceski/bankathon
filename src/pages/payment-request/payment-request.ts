/**
 * Created by denkomanceski on 5/30/17.
 */
import {Component, Optional} from "@angular/core";
import {UserService} from "../../services/user.service"
import {NavParams, ViewController} from "ionic-angular";
@Component({
  selector: 'payment-request',
  templateUrl: 'payment-request.html'
})
export class PaymentRequestComponent {
  payment: any = {};

  constructor(private view: ViewController, private params: NavParams){
    console.log("Initialized", this.params.data);
    console.log(this.params.data.userService.activePaymentPending, "???");
    //this.params.data
  }
  returnOnFailed: boolean = false;
  confirm(confirm: boolean){
    this.params.data.userService.confirmPayment(this.payment).subscribe(data => {
      this.view.dismiss();
    })
  }

  dismiss(){
    this.view.dismiss();
  }
}
