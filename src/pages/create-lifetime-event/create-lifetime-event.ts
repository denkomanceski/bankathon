/**
 * Created by denkomanceski on 5/30/17.
 */
import {Component} from "@angular/core";
import {UserService} from "../../services/user.service"
import {ViewController} from "ionic-angular";
@Component({
  selector: 'create-lifetime-event',
  templateUrl: 'create-lifetime-event.html'
})
export class CreateLifetimeEventComponent {
  constructor(private userService: UserService, private view: ViewController) {
    console.log("Initialized");
  }

  newEvent: any = {};

  save() {
    this.userService.saveLifeEvent(this.newEvent).subscribe(data => {
      this.view.dismiss();
      this.userService.getLifeEvents().subscribe(events => this.userService.lifeEvents = events);
    })
  }

  dismiss() {
    this.view.dismiss();
  }
}
