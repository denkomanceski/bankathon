import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'payment-details',
  templateUrl: 'payment-details.html'
})
export class PaymentDetails {
  payment: any = {};
  constructor(public navCtrl: NavController, private userService: UserService, private params: NavParams) {
    console.log(this.params.data, "sistemot padna.");
    this.payment = this.params.data;
  }

}
