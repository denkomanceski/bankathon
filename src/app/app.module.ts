import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {UserService} from "../services/user.service"
import {InitiatePaymentComponent } from "../pages/initiate-payment/initiate-payment"
import {ProfilePage} from "../pages/profile/profile"
import {HttpModule} from "@angular/http";
import {FilterPipe} from "../pipes/filter.pipe";
import {PaymentRequestComponent} from "../pages/payment-request/payment-request";
import {PaymentDetails} from "../pages/payment-details/payment-details";
import {CreateLifetimeEventComponent} from "../pages/create-lifetime-event/create-lifetime-event";
import {UtilService} from "../services/utilities";
import {EditProfilePage} from "../pages/edit-profile/edit-profile";
import {Facebook} from "@ionic-native/facebook";
import {PaymentPendingPage} from "../pages/payment-pending/payment-pending";
import {CloudModule} from "@ionic/cloud-angular";
import {CloudSettings} from "@ionic/cloud/dist/es5";
import {LoginPage} from "../pages/login/login";

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '06dedad7',
  },
  'push': {
    'sender_id': '255047070148',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ProfilePage,
    InitiatePaymentComponent,
    FilterPipe,
    PaymentRequestComponent,
    PaymentDetails,
    EditProfilePage,
    CreateLifetimeEventComponent,
    PaymentPendingPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings)

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    InitiatePaymentComponent,
    PaymentRequestComponent,
    PaymentDetails,
    CreateLifetimeEventComponent,
    ProfilePage,
    EditProfilePage,
    PaymentPendingPage,
    LoginPage
  ],
  providers: [
    UserService,
    UtilService,
    StatusBar,
    SplashScreen,

    Facebook,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
